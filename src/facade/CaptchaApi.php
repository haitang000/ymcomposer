<?php

namespace yuanmeng\captcha\facade;

use think\Facade;

/**
 * Class Captcha
 * @package yuanmeng\captcha\facade
 * @mixin \yuanmeng\captcha\Captcha
 */
class CaptchaApi extends Facade
{
    protected static function getFacadeClass()
    {
        return \yuanmeng\captcha\CaptchaApi::class;
    }
}
